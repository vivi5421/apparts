# St Philippe #

**MAUVAIS QUARTIER**

![](https://img1.leboncoin.fr/ad-image/edc513e62ae837462fc7e61aeac871bd91b3062e.jpg)

<https://www.leboncoin.fr/ventes_immobilieres/1630965015.htm/>

T3 Nice St Philippe
230 000 €
12/06/2019 à 10h26

Critères
Type de bien
Appartement
Pièces
3
Surface
65 m²
GES E
Classe énergie D



Description
A Vendre particulier Nice St Philippe /// Au calme dans petite copropriété entourée de 300 m2 de jardin. Pas de travaux à prévoir. Climatisation.
Salon
cuisine
Salle de bain
WC
2 chambres dont une avec placard
1 balcon
1 terrasse
2 places de parking
Idéal famille
Proche école/collège/lycée/université/Gare/Aéroport/Mer/Centre ville

Agence s'abstenir s'il vous plaît



Bonjour , le bien se situe dans le secteur de l'avenue Pierre Emmanuel . Je vous fais parvenir quelques photos supplémentaires et reste à votre disposition. Cordialemen



Particulier
06 64 72 43 10