# Mantega-Righi #

**SOUS OFFRE**

![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/6e4/6/6631d444-0d91-4c11-b600-a9a9bdd16f16.jpg)

<https://www.logic-immo.com/detail-vente-6e4d9cfc-6702-a7e1-1852-8e8cefdf3799.htm>


Descriptif du bien

    Ref de l'annonce:10732 Chauffage:Individuel, électrique Garage Terrasse/Balcon:Terrasse, balcon Etage du bien:5e Nombre d'étages du bâtiment:7 Cave Ascenseur Sécurité:Gardien Nombre de salle de bain:1 

** Exclusivité **
EXCLUSVITE 3P AVEC TERRASSE DANS RESIDENCE GRAND STANDING !!! EXCLUSIVITE : MANTEGA-RIGHI, à

proximité immédiate des commodités et dans une résidence de grand standing de 1987 en retrait des voies de circulation et avec gardien et ascenseur, magnifique 3 pièces de 68,46m² en excellent état et en étage élevé (5/7ème) dans un écrin de verdure et au calme absolu. L'appartement se compose d'une entrée avec toilette séparé, un séjour de 21,42m² avec cuisine aménagée attenante, ces 2 pièces donnant sur une grande terrasse de 10m². Coin nuit séparé avec 2 chambres de 10,88m² et 11,6lm² donnant sur un grand balcon de 8m² les reliant au séjour, une salle de bains et un dressing. Nombreuses prestations parmi lesquelles : véritable parquet dans les chambres, baies vitrées coulissantes, volets roulants électriques, électricité refaite, porte blindée, nombreux rangements, cave... Calme et sans vis-à-vis. Possibilité garage. Proximité immédiate bus et commerces. Tramway à 10mns, Centre ville ou autoroute accessible en 15mns. coup de coeur assuré !!! Quote-part annuel (2018) : 1.381,76 € soit 115,14 € / mois. nombre de lots de copropriété : 87 lots principaux, 171 lots secondaires. Réf. annonce : 10732




Bien vendu en copropriété

    N° du lot présenté : NC
    Nombre de lots : 258
    Charges prévisionnelles : 1 381 € / an
    Procédure en cours : non
    Mesure(s) en cours : NC

Consommation énergétique

    DPE D = 151 à 230 Cat. D = 174
    GES B = 6 à 10 Cat. B = 8





VOTRE AGENCE IMMO FR
04 92 10 10 12
