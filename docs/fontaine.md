# Fontaine #

**MAUVAISE ECOLE**

**VISITE 24/07/2019 18h30**

~~VISITE 23/07/2019 13h30~~

![](https://static.pap.fr/photos/C61/C61A2062.jpg)

<https://www.pap.fr/annonces/appartement-nice-r426102062>

 Nice (06000)

    3 pièces 2 chambres 60 m² 

Magnifique 3 pièces entièrement rénové de 53m2 + 6m2 de terrasse dans une résidence sécurisée avec gardien exposition sud-ouest et traversant
Secteur bas rimiez/chambrun (25avenue jean de la fontaine).

Il se compose d'un beau salon ouvert et lumineux avec vue mer, de 2 chambres avec placards de rangement ,1 salle de douche avec rangement et véritable douche à l'italienne, wc suspendus, cuisine moderne, 2 balcons dont une buanderie.

Nombreuses prestations : refait avec de beaux matériaux, porte blindée, double vitrage,volets roulants électriques, digicode, gardien ,isolation, traversant, fibre,calme et lumineux, magnifique vue mer, climatisation, nombreux rangements.

Les charges sont de 170€/mois
Comprenant eau froide et chauffage collectif
La taxe foncière est de 1006 €.

Pour compléter ce bien un grand garage de 20m2 avec carrelage, électricité ainsi qu'une mezzanine et une grande cave de 6m2.

Particulier
06.60.35.88.77

![](https://framapic.org/xffQ7nTGxtWO/tLFZORBYy4x1.jpg)
![](https://framapic.org/eObLT8oyDSx0/d9BlfwHNacnw.jpg)

  
ACACIAS MATERNELLE
123 AVENUE HENRY DUNANT -- 06100 NICE
Directeur : PINCHAUX VINCENT
Téléphone : 04 92 07 91 75
  
ACACIAS ELEMENTAIRE
123 AVENUE HENRY DUNANT -- 06100 NICE
Directeur : BOISSARD NADEGE
Téléphone : 04 92 07 91 71