# Pessicart #

**ROUTE COMPLIQUEE**

![](https://img4.leboncoin.fr/ad-image/dfa3d806f1c722cdc7872d5002242fa1bc3206e4.jpg)

<https://www.leboncoin.fr/ventes_immobilieres/1631394727.htm/>

259 000 €
11/07/2019 à 23h07

Critères
Type de bien
Maison
Pièces
3
Surface
65 m²
GES C
Classe énergie D


Description
Dans une résidence sécurisée, très calme, familiale et verdoyante, à vendre maison de 65 m2 (Carrez : 41,19 m²) située au 396 av de Pessicart, à Nice.

Orientation sud, ouest et nord. Jolie vue mer et verdure. Demi-mitoyenne, jardin 51 m2 bio depuis 12 ans, terrasse 14 m2, dépendance 7,48 m2, abri de jardin. Pas de vis-à-vis dans la maison et sur la terrasse. Voisinage très agréable.

Au rez-de-chaussée : cuisine équipée, petite salle d’eau refaite à neuf (lavabo, douche, wc, machine à laver), salon 20 m2 (dont une partie est complètement fermable pour utilisation en bureau ou chambre d’appoint).
À l’étage : deux chambres mansardées (11,99 m2 et 11,31 m2 au sol).

Très nombreux rangements. Chauffage électrique individuel (2x500whats). Cuisine : mixte gaz/électricité. Internet : fibre. Charges 125 €/mois (dont eau froide).
Piscine dans la résidence. 1 place parking (possibilité de garer 2 ou 3 voitures à l’extérieur).
Bus 63 au pied de la résidence.

À côté : école maternelle, parc de la Clua, jeux de boules. À proximité : école maternelle et primaire, médecins, dentiste, boulangerie, kinés, Centre AnimaNice, superette.

(Nous ne prendrons aucun mandat d’agences, de mandataires ou de chasseurs d’appartement.)


Particulier?
06 06 57 13 01