# Bas St-Georges #

**MAUVAISE ECOLE**

![](https://www.century21.fr/imagesBien/202/558/c21_202_558_21895_8_94E89279-4A60-4FE1-8068-1140E1CBA4A3.jpg)

<https://www.century21.fr/trouver_logement/detail/1876954886/>

**[Visite 360 degres](https://www.century21.fr/trouver_logement/detail/1876954886/video/)**

 279 000 € 

 Description Réf: 21895

NICE - BAS SAINT GEORGES / PALMIERS - Appartement 3 pièces en 3ème et Dernier étage avec 19m² de terrasse dans copropriété sécurisée livrée en 2014. Triple exposition, séjour sud et ouest, environnement calme, vue dégagée verdure. Un grand garage pour une voiture et un deux-roues complète ce bien.
Fiche détaillée du bien immobilier
Vue globale

    Type d'appartement : T3
    Surface totale : 60,1 m2
    Surface habitable : 60,1 m2
    Nombre de pièces :3 [Voir en détail]
    Type de construction : Traditionnelle
    Année construction : 2014

Equipement

    Porte : Serrure 5 points
    Isolation : Murs et double vitrage
    Chauffage : Individuel électrique individuel électrique electricité
    Eau chaude : Individuelle solaire
    Toiture : Terrasse
    Ascenseur
    Interphone

Les Plus

    garage
    Terrasse
    Dégagée verdure

À savoir

    Charges / mois : 175 €
    Taxe foncière : 1261 €

Les performances énergétiques

    38

    1

Tout savoir sur les diagnostics

    Le Diagnostic de Performance Energétique
    Diagnostics liés à la transaction
    Diagnostics liés à la location
    Installation électrique

Copropriété

    Nombre de Lots : 23
    Charges courantes par an : 2098,0 €
    Pas de procédure en cours



## Adresse ##
Avenue Henry Musso

## Ecoles ##
ACACIAS MATERNELLE
123 AVENUE HENRY DUNANT -- 06100 NICE
Directeur : PINCHAUX VINCENT
Téléphone : 04 92 07 91 75
  
ACACIAS ELEMENTAIRE
123 AVENUE HENRY DUNANT -- 06100 NICE
Directeur : BOISSARD NADEGE
Téléphone : 04 92 07 91 71

