# Acropolis #

**MAUVAIS QUARTIER**

![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/88d/c/c6dd59dc-023c-469b-9a7a-511b822d8cfa.jpg)

<https://www.logic-immo.com/detail-vente-88d44f9c-633c-e6fa-9509-d8c2961d1059.htm>


Appartement
63m² 3 pièces
NICE (06000) | Acropolis
268 000 €
ou 1 075 € / mois*



Descriptif du bien

    Ref de l'annonce:2868751 Chauffage:Au gaz Jardin Ascenseur Accès handicapé Nombre de salle de bain:1 

** Exclusivité **
3 Pièces Bas Cimiez Exclusivité, bas CIMIEZ, dans une résidence

sécurisée, beau 3 pièces en Rez de jardin. Ses atouts; environement calme, appartement en parfait état, traversant, cave. Un appartement idéal jeune couple... Possibilité d'un Garage en supplément dans la résidence. Honoraires à la charge du vendeur, 128 lots de copropriété. Pour tous renseignements, contactez Michael BLANDIN : +33 (0)6 09 07 65 98 Réf. annonce : 2868751

onsommation énergétique

    DPE C = 91 à 150 Cat. C = 121
    GES D = 21 à 35 Cat. D = 28



PARTNERSIMMO - Nice
04 93 56 60 48
