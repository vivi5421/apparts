# Bd Las PLanas #

**MAUVAIS QUARTIER**

![](https://img6.leboncoin.fr/ad-image/8a86d628f7bf4087302b314095d9e960c93f9e46.jpg)

<https://www.leboncoin.fr/ventes_immobilieres/1648852101.htm/>

275 000 €
21/07/2019 à 10h25

Critères
Type de bien
Appartement
Pièces
3
Surface
75 m²
GES
Non renseigné
Classe énergie
Non renseigné


Description
Nice nord - Collines St Sylvestre
Beau 3 pièces traversant de 75m2, rénové avec goût
Grande pièce à vivre avec cuisine ouverte, aménagée et équipée, donnant sur un jardin surélevé et sans vis-à-vis de 70m2.
2 belles chambres
Salle de bain comprenant une douche à l'italienne
Toilettes indépendants
Le bien est complété d'un grand garage et d'une cave.

AGENCES S'ABSTENIR


