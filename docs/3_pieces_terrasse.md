# Nice Cimiez 3 pièces Terrasse #

**VISITE 25/07/2019 18h30**

**DEMANDER PLAN DE MASSE**


| Surface | Prix    | Pièces | Parking   |
| ------- | ------- | ------ | --------- |
| 56      | 257 000 | 2      | Collectif |


![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/7178625885d14dac0ae35a0.74380756_1920.jpg)

<http://www.cimiez-residences.fr/fr/recherche/vente-appartement-3-pieces-nice-cimiez-06000-3507534>

Description

Ref. 3171352 - Dans résidence sécurisée, au calme, 3 pièces composé d'un vaste séjour de 28m2 avec cuisine ouverte donnant sur une belle terrasse de 23m2, 2 chambres, salle de bains, wc séparé, cave
Facilité de stationnement dans parking commun
Charges annuelles 3.072€

Bien soumis au statut de la copropriété (loi du 10 juillet 1965).
Résumé

    Pièces 3 pièces
    Surface 56 m²
    Chauffage Convecteur, Gaz
    Eau chaude Chaudière
    Etage 1er / 4 étages
    Exposition Ouest

Surfaces

    1 Séjour 28.52 m²
    2 Chambres 10.03 m², 7.54 m²
    1 Terrasse 23.41 m²
    1 Cave

Prestations

    Air conditionné
    Double vitrage
    Fenêtres coulissantes
    Stores électriques
    Volets roulants électriques
    Éclairage extérieur
    Ascenseur
    Gardien

Efficacité énergétique
Énergie - Consommation conventionnelle Énergie - Estimation des émissions
Informations légales

    Honoraires à charge vendeur
    Taxe foncière896 €
    Charges de copropriété240 € / Mois
    Loi Carrez56.04 m²
    Consultez notre barème d'honoraires
    Pas de procédure en cours




### Adresse ###

19 Avenue de Valombrose

### Ecoles ###
BELLANDA MATERNELLE
6 AVENUE BELLANDA -- 06000 NICE
Directeur : DORVEAUX ANNE
Téléphone : 04 93 53 56 30
  
CIMIEZ ARENES ELEMENTAIRE I
0 AVENUE MONTE CROCE -- 06000 NICE
Directeur : CLOT BERNARD
Téléphone : 04 93 81 48 58
  
CIMIEZ ESSLING PRIMAIRE II
1 AVENUE SALONINA -- 06000 NICE
Directeur : SAIMI LEILA
Téléphone : 04 92 00 79 71

College Matisse

Lycee Calmette



## Contact ##

CIMIEZ RESIDENCES

12, avenue Cap de Croix
06100 Nice
France

Téléphone : +33 (0)9 54 04 98 11

## Photos ##
![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/7178625885d14dac0ae35a0.74380756_1920.jpg)
![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/666763935d14dabeddc596.59830776_1920.jpg)
![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/2587037095d14dabd077df0.23994325_1920.jpg)
![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/9919320945d14dabb30f341.22501361_1920.jpg)
![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/14613545295d14dab9463c14.42767562_1920.jpg)
![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/6165300065d14dab781d9f7.73380780_1920.jpg)
![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/8955342535d14dab61a3374.36720392_1920.jpg)
![](https://d1qfj231ug7wdu.cloudfront.net/pictures/estate/3172/3171352/5549098275d14dab418a736.13111408_1920.jpg)
