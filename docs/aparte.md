# L'Apparté #

**OFFRE FAITE 25/07/2019 VALIDEE**

**VISITE 25/07/2019 16h30**

Surface | Prix | Pièces | Parking
- | - | - | -
62.59m2 | 295 000 euros | 3 | Box

![](https://v.seloger.com/s/width/800/visuels/0/d/j/u/0djul9wj4w93ip3if2x3ykbzktduffoecuwqi7s9k.jpg)


<https://www.seloger.com/annonces/achat/appartement/nice-06/cessole/149780097.htm?enterprise=0&garden=1&natures=1,2,4&places=%5b%7bci%3a60088%7d%5d&price=NaN%2f300000&projects=2&qsversion=1.0&rooms=3,4&sort=d_dt_crea&surface=60%2fNaN&types=1,2,9,13,14&bd=ListToDetail>

Frais notaires: 2 784 €

Appartement

    3 pièces 1 chambre 62,59 m² 

295 000 €
À partir de 1 182 € / mois1

Nice

Réf: GB00095436

    Le bienL'agenceLe quartierVotre projet

logo agence
NEXITY Nice Saint-Sylvestre
Contactez l'agence par email
Avez-vous un bien a vendre ?Oui Non
Une précision ? (facultatif)
Je ne souhaite pas recevoir les annonces similaires et les suggestions personnalisées de SeLoger. En savoir plus

Pour vous accompagner dans votre recherche, vous pourrez également recevoir par email ou notification dans votre application mobile SeLoger, les annonces immobilières similaires correspondant à vos critères de recherche ainsi que nos propositions de services autour de votre projet immobilier. Dans ce cadre, vos coordonnées ne sont pas communiquées à des tiers.
A propos de cet appartement 3 pièces à Nice
L'avis du professionnel

Nice Saint barthelemydans résidence neuvemagnifique T3 en rez-de-jardin comprenant: cuisine, séjour, 2 chambres donnant sur terrasse et jardin, salle de bain, RANGEMENTS.UN petit écrin de verdure au calme du bruit en pleine ville !!
Les +
1 Terrasse
Orientation Ouest
Général
Surface de 62,59 m²
Au rez-de-chaussée
3 Pièces
1 Chambre
A l'intérieur
Rangements
Séjour
Autres
Calme
Les diagnostics énergétiques
DPE Non communiqué
À propos du prix

295,000 €
Les honoraires sont à la charge du vendeur.



Appartement acheté 338 000 euros par couple qui divorce et revend



## Adresse ##
[78 avenue Cyril Besset](https://www.google.com/maps/place/78+Avenue+Cyrille+Besset,+06100+Nice/@43.7187227,7.2512253,17z/data=!3m1!4b1!4m5!3m4!1s0x12cdcfe4a60201b7:0xbd5958942153944!8m2!3d43.7187227!4d7.253414)



## Ecoles ##
SAINT BARTHELEMY MATERNELLE
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : SACCOMANO CATHY
Téléphone : 04 92 07 86 44
  
SAINT BARTHELEMY ELEMENTAIRE I APPL.
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : JEUNOT FABRICE
Téléphone : 04 92 07 86 31
  
SAINT BARTHELEMY ELEMENTAIRE II
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : PICONNIER BRIGITTE
Téléphone : 04 92 07 86 41


College Fabre

Lycee Imperial


## Contact ##
Nexity - 06 68 01 73 64
eblas@nexity.fr

## Photos ##
![](https://v.seloger.com/s/width/800/visuels/0/d/j/u/0djul9wj4w93ip3if2x3ykbzktduffoecuwqi7s9k.jpg)
![](https://v.seloger.com/s/width/800/visuels/0/f/9/9/0f9998qm28lkzbep1ib5vqefkhf4niegf2nsoqrbs.jpg)
![](https://v.seloger.com/s/width/800/visuels/1/t/x/4/1tx4gva8c3tp4vtjs0v3lwguydf266y7cgkzi48ry.jpg)
![](https://v.seloger.com/s/height/800/visuels/1/y/c/m/1ycm38nrvizgw7ngfgqs8adhzxctibb439oto7uqz.jpg)
![](../static/vivaldi_1e18pSHxuQ.png)

## Questions ##
- prise de courant dans box ?
- dimensions box?

## Parkings ##
Numeros 192

Achat 201 pour 30 000 euros

## Documents ##
[Résidence Aparté](../static/residence_aparte.pdf)

[Lot 5009](../static/lot_5009.pdf)

[Lot 5009 v2](../static/plan_5009_nb.pdf)

[Plan sous sol Aparté](../static/plan_sous_sol_aparte.pdf)

[Devis Machine à laver dans WC](../static/20082019_Chiffrage_1_TMA_5009.pdf)