# Libération, Quartier de l’Evêché #

**Visite le 24/07/2019 à 17h30**

**SOUS COMPROMIS JUSQU'AU 06/08/2019 (3 refus de prêts)**

| Surface | Prix    | Pièces | Parking |
| ------- | ------- | ------ | ------- |
| 66      | 298 000 | 2      |         |


![](https://img6.leboncoin.fr/ad-image/13b13a8ee0f1ec66715d766892347995169595c0.jpg)

<https://www.leboncoin.fr/ventes_immobilieres/1649468730.htm/>




298 000 €
22/07/2019 à 17h09
Critères
Type de bien
Appartement
Pièces
2
Surface
66 m²
GES E
Classe énergie C


Description
Rare !
Situé moins de 5 minutes à pied du tramway Borriglione, de la crèche et du marché de la Libération dans une petite rue au calme.
Spacieux rez-de-chaussée surélevé de 66 m², traversant Nord-Sud avec rez-de-jardin à l'arrière. Grande chambre de 18 m² avec balcon de 4m² au sud.
Une pièce de vie de 43 m² avec cuisine ouverte aménagée. Facilement ré-aménageable en 3 pièces. Beaux volumes ouverts sur un extérieur de 49m² calme, arboré et sécurisé.
Cave de plain-pied. Immeuble bien entretenu avec gardienne. Local à vélos et poussettes fermé.
Charges 280 euros/mois incluant l’immeuble, le chauffage et l’eau chaude et froide.
Hall d’entrée fraîchement rénové. Rénovation parties communes en étage votée et payée. Diagnostics disponibles.
Très bon état (repeint il y a un an) et sans travaux.

Syndicat: AEGIP
Taxe foncxiere: 1118 euros

## Adresse ##
4 rue Cavendish
<https://goo.gl/maps/ieVkYaprmwgSsXoc8>

## Ecoles ##
SAINT BARTHELEMY MATERNELLE
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : SACCOMANO CATHY
Téléphone : 04 92 07 86 44
  
SAINT BARTHELEMY ELEMENTAIRE I APPL.
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : JEUNOT FABRICE
Téléphone : 04 92 07 86 31
  
SAINT BARTHELEMY ELEMENTAIRE II
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : PICONNIER BRIGITTE
Téléphone : 04 92 07 86 41

College Valeri

Lycee Imperial


## Contact ##
Particulier Mme Sanchis
06 03 98 29 63

## Photos ##
![](https://img6.leboncoin.fr/ad-large/13b13a8ee0f1ec66715d766892347995169595c0.jpg)
![](https://img0.leboncoin.fr/ad-large/d7aa5a5ade8efe1207279345b690273d04d6196c.jpg)
![](https://img4.leboncoin.fr/ad-large/85f5161960b80e898923819dc4cc89627e000dc5.jpg)
![](https://img5.leboncoin.fr/ad-large/97332d3d500f6cbbb1905981a49d001e585e8898.jpg)
![](https://img5.leboncoin.fr/ad-large/93031448a0223ac690c3f7074733e39fe4fef49a.jpg)
![](https://img2.leboncoin.fr/ad-large/e6c7c2348dc272135c3c99d88f0cb3abdd063fca.jpg)
![](https://img0.leboncoin.fr/ad-large/176f209aa2ba26c94cb27a88eb40d095144f3259.jpg)
![](https://img5.leboncoin.fr/ad-large/8781356d623799d7e90a8d0a1c827ea22c322c60.jpg)


## Documents ##
[Diagnostiques](https://app.box.com/s/ra5y69q0bu3a8d2eii1ovdatbzq7lnc6)

[PV 2019](https://app.box.com/s/6vd3tfsuq0e9mkboouz2h1ezlmpim7bb)

[PV 2018](https://app.box.com/s/9ufoez3wi1gf2xtper2st02smtgjtebp)

[PV 2017](https://app.box.com/s/d89jagq2p1t9eehh8b384rsrd4195zat)
