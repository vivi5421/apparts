# 59m² Nice - Cimiez #

**SOUS COMPROMIS**


| Surface | Prix    | Pièces | Parking     |
| ------- | ------- | ------ | ----------- |
| 59      | 298 000 | 2      | 1 exterieur |

![](https://v.seloger.com/s/width/800/visuels/0/2/t/n/02tn6ctwidogzqzrsu6nhw8heg8xynvmjnllo3di8.jpg)

<https://www.seloger.com/annonces/achat/appartement/nice-06/cimiez/148108681.htm?types=1,2&projects=2&enterprise=0&garden=1&terrace=1&cellar=1&natures=1,2,4&price=200000/NaN&surface=45/NaN&rooms=2,3,4,5&places=[{ci:60088}]&qsVersion=1.0&BD=Carto_Detail_Cartouche,Carto_Detail_Cartouche&BD=Carto_Detail_Cartouche&ref=map>

L'avis du professionnel

CIMIEZ: EXCLUSIVITÉ ! RARE ! Vaste 2 pièces à vendre dans une belle résidence de standing. 59 m². Terrasse. Grand Jardin. Calme. Plein Sud. Parking privatif. Cave.
Les +
Ascenseur
Cave
1 Terrasse
Orientation Sud
Général
Surface de 59 m²
Année de construction 1983
Bâtiment de 5 étages
Au rez-de-chaussée
2 Pièces
1 Chambre
A l'intérieur
Chauffage individuel
A l'extérieur
1 Parking
Autres
Interphone
Digicode
Calme



Appartement

    2 pièces 1 chambre 59 m² 

298 000 €
À partir de 1 194 € / mois1

Nice

Réf: 655
Exclusivité

    Le bienL'agenceLe quartierVotre projet

logo agence
IMMO DE FRANCE COTE D'AZUR
17
Contactez l'agence par email
Avez-vous un bien a vendre ?Oui Non
Une précision ? (facultatif)
Je ne souhaite pas recevoir les annonces similaires et les suggestions personnalisées de SeLoger. En savoir plus

Pour vous accompagner dans votre recherche, vous pourrez également recevoir par email ou notification dans votre application mobile SeLoger, les annonces immobilières similaires correspondant à vos critères de recherche ainsi que nos propositions de services autour de votre projet immobilier. Dans ce cadre, vos coordonnées ne sont pas communiquées à des tiers.

## Adresse ##

**16 avenue de flirey** en dessous de la pharmacie de cimiez

## Ecoles ##
BELLANDA MATERNELLE
6 AVENUE BELLANDA -- 06000 NICE
Directeur : DORVEAUX ANNE
Téléphone : 04 93 53 56 30
  
CIMIEZ ARENES ELEMENTAIRE I
0 AVENUE MONTE CROCE -- 06000 NICE
Directeur : CLOT BERNARD
Téléphone : 04 93 81 48 58
  
CIMIEZ ESSLING PRIMAIRE II
1 AVENUE SALONINA -- 06000 NICE
Directeur : SAIMI LEILA
Téléphone : 04 92 00 79 71

College Matisse

Lycee Calmette


## Contact ##
IMMO DE FRANCE COTE D'AZUR 
06 03 32 27 05
florent.vente@immodefrance.com

## Photos ##
![](https://v.seloger.com/s/width/800/visuels/0/2/t/n/02tn6ctwidogzqzrsu6nhw8heg8xynvmjnllo3di8.jpg)
![](https://v.seloger.com/s/width/800/visuels/0/b/9/2/0b92ljv9jqk6vbs2vvd8i7xt4l7t695iamnlrkkdc.jpg)
![](https://v.seloger.com/s/width/800/visuels/1/e/8/o/1e8okpq9dcmrkdlxowx8e3hp7rxotzqg3w1xoxb8g.jpg)
![](https://v.seloger.com/s/height/800/visuels/0/g/x/6/0gx67h4ww2n81bgdfuxajicvxp50ebmy3c2lskti0.jpg)
![](https://v.seloger.com/s/width/800/visuels/2/6/h/9/26h9w9lz5cy47icn9ox76u2b1kg95nuk040fdplpc.jpg)
![](https://v.seloger.com/s/height/800/visuels/0/x/2/j/0x2jujwa0ekk6u73v6yfnnz64411yx7i0lwmyzihk.jpg)
![](https://v.seloger.com/s/height/800/visuels/1/9/j/7/19j7od9lknv4a17qo4zyfq25fyhy6797ndcf6udlk.jpg)
![](https://v.seloger.com/s/height/800/visuels/1/d/x/s/1dxsd45tgqfmwqp49nq57v7qgt7swyryyv30noqc8.jpg)
![](https://v.seloger.com/s/width/800/visuels/1/f/4/u/1f4u166chqbpdohuy76fj7n210df1rmnl69h4kffk.jpg)
![](https://v.seloger.com/s/height/800/visuels/1/f/l/e/1fle2c3wyhuke38y6gef6js4t1o3lalc8y864t07s.jpg)
