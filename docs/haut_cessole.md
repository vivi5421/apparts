# Hauts de Cessole #

**MAUVAIS QUARTIER**

| Surface | Prix          | Pièces |
| ------- | ------------- | ------ |
| 63m2    | 269 000 euros | 3      |

![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_8_ACE8CF37-8209-4C53-920B-0EF4DBAFCCCE.jpg)

<https://www.century21.fr/trouver_logement/detail/1775929248/>

<https://www.leboncoin.fr/ventes_immobilieres/1528569010.htm/>

 269 000 € 

 Description Réf: 23784

NICE NORD HAUT DE CESSOLE- dans une impasse au calme absolu, agréable F3 de 64m2, dans une copropriété de bon standing et proche de toutes les commodités. Appartement soigneusement entretenu et très lumineux avec son balcon de 5 m2 et sa terrasse-jardin de près de 90m2. Garage et cave. Un bien rare sur le secteur à visiter sans tarder !!!
Fiche détaillée du bien immobilier
Vue globale

    Type d'appartement : F3
    Surface totale : 63,6 m2
    Surface habitable : 63,6 m2
    Nombre de pièces :3 [Voir en détail]
    Type de construction : Traditionnelle
    Année construction : 1980

Equipement

    WC séparés
    Porte : Serrure 3 points
    Isolation : Double vitrage
    Chauffage : Individuel électrique individuel électrique electricité
    Eau chaude : Cumulus
    Toiture : Terrasse
    Balcon
    Véranda
    Interphone

Les Plus

    box ferme
    Terrasse
    Jardin

À savoir

    Charges / mois : 170 €
    Taxe foncière : 1209 €

Les performances énergétiques

    160

    30

Tout savoir sur les diagnostics

    Le Diagnostic de Performance Energétique
    Diagnostics liés à la transaction
    Diagnostics liés à la location
    Installation électrique

Copropriété

    Nombre de Lots : 53
    Charges courantes par an : 2040,0 €
    Pas de procédure en cours


## Adresse ##
passage saint hubert

## Ecoles ##
  
SAINT SYLVESTRE MATERNELLE
0 CHEMIN DE LA PASSERELLE -- 06100 NICE
Directeur : VATRINET NATHALIE
Téléphone : 04 93 84 21 94
  
SAINT SYLVESTRE ELEMENTAIRE I
167 AVENUE CYRILLE BESSET -- 06100 NICE
Directeur : SACCOMANO POLLINA SYLVIE
Téléphone : 04 97 03 12 01
  
SAINT SYLVESTRE ELEMENTAIRE II
167 AVENUE CYRILLE BESSET -- 06100 NICE
Directeur : SILVERA HISTRE CAROLINE
Téléphone : 04 97 03 12 06


## Contact ##
Century 21
04 97 14 84 00 


## Photos ##
![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_1_ACE8CF37-8209-4C53-920B-0EF4DBAFCCCE.jpg)
![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_1_36B35C68-1034-452B-AF99-56D69DB29630.jpg)
![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_1_8F085D48-ECA5-43F9-AF74-D9906B67BEE4.jpg)
![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_1_E46381F1-81F9-4BC2-AE3D-259AA21F3F73.jpg)
![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_1_A210A63C-1EE2-4033-BEF3-2FB282F7213E.jpg)
![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_1_4A1338A1-7196-4B99-B997-75FA14E3A028.jpg)
![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_1_4B88AE34-E951-48D6-800C-7313B6CC02AE.jpg)
![](https://www.century21.fr/imagesBien/202/1138/c21_202_1138_23784_1_B9521AE6-121B-48FF-8020-8A3CA95942B3.jpg)