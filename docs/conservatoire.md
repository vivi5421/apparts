# Conservatoire #

**MAUVAIS QUARTIER**

| Surface | Prix    | Pièces |
| ------- | ------- | ------ |
| 62      | 215 000 | 3      |

![](https://www.bosse.fr/Repo/biens/78/photos/506.jpg)

<https://www.bosse.fr/immobilier-appartement-france-nice-appartement-3-pieces-62-m-a-vendre--78.html>

 1 (Réf: FR383840) 

215 000 euros
62 mêtre carré
3 Pièce(s)


CIMIEZ - PROCHE CONSERVATOIRE
Dans une résidence bien entretenue, beau 3 pièces en excellent état et en étage élevé.
Il se compose d'une entrée, d'un séjour donnant sur une terrasse avec vue dégagée, d'une cuisine indépendante équipée, de deux chambres avec rangements, d'une salle de bains, WC indépendants et un grand dressing.
Vous serez séduits par cet appartement lumineux et au calme niché dans la verdure.
Proche de toutes les commodités et écoles.
Un grande cave saine et un parking complètent ce bien.
Climatisation.
A visiter sans tarder ..... 


2 Chambre(s)Etage : 12/151 Salle(s) de bainTerrasse : 6m²1 Parking(s) ExtérieurVue : dégagéeCharges / Mois 200 €


Copropriété : Oui Nb de lots : 490 N° de Lot : 0 Quote-part : 2400
200 € Charges
1 000 € Taxe foncière 

## Contact ##
Gilles Bossé Immobilier
6 avenue Alfred de Vigny
06100 Nice
04 92 07 92 07

## Photos ##
![](https://www.bosse.fr/Repo/biens/78/photos/507.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/506.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/508.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/514.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/513.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/509.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/510.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/511.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/512.jpg)
![](https://www.bosse.fr/Repo/biens/78/photos/515.jpg)