# Rimiez #

**SOUS COMPROMIS**

**MAUVAISE ECOLE**

![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/ec2/4/4a9e759a-5fc9-4deb-a667-578bff62e827.jpg)

<https://www.logic-immo.com/detail-vente-ec2ce838-56c0-2b2b-e7eb-66787c08f211.htm>

<https://www.bosse.fr/immobilier-appartement-france-nice-appartement-3-pieces-6337-m-a-vendre-175.html>


Appartement
63m² 3 pièces
NICE (06100) | Rimiez
230 000 €
ou 923 € / mois*



Descriptif du bien

    Ref de l'annonce:1291T Chauffage:Individuel, électrique Garage Terrasse/Balcon:Terrasse Jardin Etage du bien:1er Nombre d'étages du bâtiment:4 Cave Ascenseur 

NNice Nord RIMIEZ à vendre dans résidence calme et fleurie, 3 pièces de 63

.37m² avec cave et garage en sous sol. Appartement traversant avec cuisine semi ouverte sur le séjour, donnant sur une terrasse de 15m² avec une jolie vue dégagée sur collines, ainsi que 2 chambres donnant sur un jardinet de 10m². Bus au pied de l'immeuble. idéal jeune couple à visiter rapidement! A rénover nombre de lots: 202 Charges annelles: 1860€ Les honoraires seront intégralement à la charge des vendeurs. Les honoraires seront intégralement à la charge des vendeurs. Réf. annonce : 1291T



Bien vendu en copropriété

    N° du lot présenté : NC
    Nombre de lots : 280
    Charges prévisionnelles : Nous consulter / an
    Procédure en cours : NC
    Mesure(s) en cours : NC

Consommation énergétique

    DPE C = 91 à 150 Cat. C = 109
    GES A = 0 à 5 Cat. A = 4


## Adresse ##
avenue Henri Musso
<https://www.google.com/maps/place/19+Avenue+Henry+Musso,+06100+Nice/@43.7340769,7.2704453,3a,75y,279.11h,90t/data=!3m7!1e1!3m5!1sAyepU1W3kp6ITi71u2rFMQ!2e0!6s%2F%2Fgeo0.ggpht.com%2Fcbk%3Fpanoid%3DAyepU1W3kp6ITi71u2rFMQ%26output%3Dthumbnail%26cb_client%3Dsearch.TACTILE.gps%26thumb%3D2%26w%3D86%26h%3D86%26yaw%3D279.10696%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192!4m13!1m7!3m6!1s0x12cdc561531e7ffd:0xb33823f4d3f671d6!2sAvenue+Henry+Musso,+06100+Nice!3b1!8m2!3d43.734805!4d7.2703694!3m4!1s0x12cdc56129846a5f:0x4aefa63388a962ce!8m2!3d43.7340905!4d7.2703428>

## Ecoles ##
ACACIAS MATERNELLE
123 AVENUE HENRY DUNANT -- 06100 NICE
Directeur : PINCHAUX VINCENT
Téléphone : 04 92 07 91 75
  
ACACIAS ELEMENTAIRE
123 AVENUE HENRY DUNANT -- 06100 NICE
Directeur : BOISSARD NADEGE
Téléphone : 04 92 07 91 71


## Contact ##
GILLES BOSSE IMMOBILIER
04 92 07 92 07


## Photos ##
![](https://www.bosse.fr/Repo/biens/175/photos/1327.jpg)
![](https://www.bosse.fr/Repo/biens/175/photos/1328.jpg)
![](https://www.bosse.fr/Repo/biens/175/photos/1329.jpg)