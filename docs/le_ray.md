# Le Ray #

**MAUVAIS QUARTIER**

| Surface | Prix          | Pièces |
| ------- | ------------- | ------ |
| 72m2    | 226 000 euros | 4/5    |

![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/b/b62f2199-1b6e-4978-a6c3-83b68347b0fa.jpg)

<https://www.logic-immo.com/detail-vente-5d54f92b-00ae-f05e-aad7-3d42fc0e9ccc.htm>


226 000 €


Descriptif du bien

    Ref de l'annonce:10719 Chauffage:Individuel, au gaz Parking Terrasse/Balcon:Balcon Etage du bien:2e Nombre d'étages du bâtiment:4 Cave Nombre de salle de bain:1 

NICE-NORD Le Ray NICE-NORD Le Ray : Appartement d'angle 4/5 pièces (triple exposition) de

72m², avec cave et place de parking. Cet appartement est composé d'une entrée, d'un double séjour de 31m², d'une cuisine indépendante (possibilité US), de trois chambres, d'une salle de bains et d'un WC indépendant. Grand balcon devant séjour et cuisine. Prévoir travaux de modernisation. Copropriété bien tenue. Vues dégagées. Proximité écoles, commerces et tramway. Nombre de lots : NC ; Charges annuelles : 1.600 euros. Réf. annonce : 10719

2eme etage sans ascenseur

## Adresse ##
rue Charles Beaudelaire

## Ecoles ##
  
RANCHER ROSALINDE MATERNELLE
20 VIEUX CHEMIN DE GAIRAUT -- 06100 NICE
Directeur : MARTINEZ GILDA
Téléphone : 04 92 07 54 91
  
RANCHER ROSALINDE ELEMENTAIRE II
22 VIEUX CHEMIN DE GAIRAUT -- 06100 NICE
Directeur : DE SWARTE GREGORY
Téléphone : 04 92 07 54 81
  
RANCHER ROSALINDE PRIMAIRE I
24 VIEUX CHEMIN DE GAIRAUT -- 06100 NICE
Directeur : PIDON JACQUES
Téléphone : 04 92 07 54 86


## Contact ##
VOTRE AGENCE IMMO FR
04 92 10 10 12

## Photos ##
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/b/b62f2199-1b6e-4978-a6c3-83b68347b0fa.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/d/d5767a30-bb40-4504-bb14-d0d774389af8.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/9/9739357f-fa68-4b90-b5c4-795e4dcbb167.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/b/b0fcb982-e402-4ce8-8141-27fd45033581.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/9/9515b6a4-6276-48d1-b39f-050b9fa18d21.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/1/19c1f352-2000-49bc-8e2c-a7b074cf18d9.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/7/75d20887-86aa-4038-9705-8dc0f9916eba.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/1/19e3c1ed-43c7-4d87-af6f-e3adb7c5b9db.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/e/efa76b11-a253-4c5a-9e6a-274335fe0ee2.jpg)
![](https://mmf.logic-immo.com/mmf/ads/photo-prop-800x600/5d5/c/c02b3d81-6f26-4897-9f00-682823addd3a.jpg)