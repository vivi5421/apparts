# Appartement 68m² Nice - cimiez #

**CHER - PAS EXTERIEUR**

![](https://v.seloger.com/s/height/800/visuels/0/e/9/u/0e9umsh5n9kfb09h9nmpiyyv72hi7ftre08mjhie0.jpg)

<https://www.seloger.com/annonces/achat/appartement/nice-06/cimiez/148831079.htm?enterprise=0&natures=1,2,4&places=%5b%7bci%3a60088%7d%5d&price=277000%2f277000&projects=2,5&qsversion=1.0&types=1,2&bd=ListToDetail>

Appartement

    3 pièces 2 chambres 68 m² 

277 000 €
À partir de 1 110 € / mois1

Nice

Réf: 340934221469


A vendre au coeur de la colline de Cimiez, quartier résidentiel haut de gamme et verdoyant, à seulement 5 minutes du centre ville de Nice. Vous serez entourés de nombreux petits commerces de quartier, de plusieurs écoles dont le collège Matisse, de cabinets médicaux ainsi que de la clinique Saint George. Vous pourrez également vous promener au magnifique parc historique des arènes de Cimiez. L'appartement de 68 m² est situé au premier étage d'un petit immeuble, il est composé des 2 chambres avec parquet au sol, d'une cuisine indépendante, d'une salle de bain entièrement rénovée avec sa douche à l'italienne et d'un toilette indépendant. Le salon et la cuisine donnent sur un balcon de 7 m². Le salon est climatisé, et l'appartement reste calme grâce à ses fenêtres acoustiques. Le bien est vendu avec sa cave. Il est possible de se garer dans les rues adjacentes à la résidence, ou de louer une place de parking en sus. Nombre de lots de la copropriété: 21, Montant moyen annuel de la quote-part de charges (budget prévisionnel): 2900 euros soit 241 euros par mois. Les honoraires sont à la charge du vendeur. Réseau Immobilier CAPIFRANCE - Votre agent commercial Caroline LECLERCQ Plus d'informations sur le site de CAPIFRANCE (réf. 625720).



Les +
Cave
1 Balcon
Orientation Nord, Ouest
Général
Surface de 68 m²
Année de construction 1970
Au 1er étage
3 Pièces
2 Chambres
A l'intérieur
1 Salle de bain
1 Salle d'eau
1 Toilette
Toilettes Séparées
Parquet
Cuisine séparée
Séjour de 22 m²
A l'extérieur
1 Parking
Autres
Interphone
Calme


## Ecoles ##
Arenes de Cimiez

## Contact ##
LECLERCQ Caroline - Capifrance - Agent commercial 
06 24 70 40 09
