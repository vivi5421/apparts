# 10 Lange #

**doit me rappeler d ici 48h**

| Surface | Prix    | Pièces | Parking     |
| ------- | ------- | ------ | ----------- |
| 80      | 264 900 | 3      | 1 exterieur |

![](https://img5.leboncoin.fr/ad-image/b33081be8eac1037fe426a5539f46d142374e0bc.jpg)

<https://www.leboncoin.fr/ventes_immobilieres/1637668294.htm>

Appartement 3 pièces 80 m²
264 900 €
25/06/2019 à 15h59

ritères
Type de bien
Appartement
Pièces
3
Surface
80 m²
Référence
4398-0
GES F
Classe énergie E


Description
BAS DE MAISON AVEC JARDIN PARKING - NICE NORD

Bel appartement 3 pièces de 80m2 dans une maison en copropriété. L'appartement se trouve au rez de jardin d'une maison des années 60, dans une rue calme et résidentielle de Nice Nord. Toutes les pièces donnent sur un beau jardin plat de 100m2 environ. Place de parking. Travaux à prévoir. Copropriété de 2 lots (Pas de procédure en cours).
Référence annonce : 4398-0
Les honoraires sont à la charge du vendeur

A propos de la copropriété :
Nombre de lots : 2


## Adresse ##
10 rue Lange
https://www.google.fr/maps/place/10+Rue+Lange,+06100+Nice/@43.7134902,7.2517398,17z/data=!3m1!4b1!4m5!3m4!1s0x12cdd01d5767f4fb:0x7e5f0f4fab640c96!8m2!3d43.7134902!4d7.2539285

## Ecoles ##
SAINT BARTHELEMY MATERNELLE
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : SACCOMANO CATHY
Téléphone : 04 92 07 86 44
  
SAINT BARTHELEMY ELEMENTAIRE I APPL.
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : JEUNOT FABRICE
Téléphone : 04 92 07 86 31
  
SAINT BARTHELEMY ELEMENTAIRE II
17 AVENUE FRANCOIS BOTTAU -- 06100 NICE
Directeur : PICONNIER BRIGITTE
Téléphone : 04 92 07 86 41

College Valeri

Lycee Imperial


## Contact ##
L'ADRESSE - C.E.C.
04 93 52 68 68

## Photos ##
![](https://img5.leboncoin.fr/ad-large/b33081be8eac1037fe426a5539f46d142374e0bc.jpg)
![](https://img5.leboncoin.fr/ad-large/0885cce106dae930c967ab93f96d68094d8a5caa.jpg)
![](https://img5.leboncoin.fr/ad-large/aeeca52da09ecfbd3d9675d9b6d9a83a70997224.jpg)
![](https://img0.leboncoin.fr/ad-large/78f5238e111496b4e76587a0990eb21fc3eaef22.jpg)
![](https://img0.leboncoin.fr/ad-large/75359c43c5ebbd6b501cfacde16a02fe32cb741e.jpg)
![](https://img6.leboncoin.fr/ad-large/b1452497cc2fb521e0db5551e407a8d3e9e16fab.jpg)
![](https://img3.leboncoin.fr/ad-large/a2ef52535b00d5cbe3e5f2a1546987df6b9cd7ae.jpg)
![](https://img6.leboncoin.fr/ad-large/8385756c6a8288fbf512b48cbf5a1acc918df302.jpg)
