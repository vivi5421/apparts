# Maison Valrose #

**DEJA VENDU**

![](http://archimo-nice.com/_img/201706301635/photo_1.jpg)

<http://archimo-nice.com/ads_detail.php?type_search=maison&id_ads=190>

Nice
3 Chambres
75 m2
294 000 €
Localiser
Imprimer
VENDU

MAISON INDIVIDUELLE de 74,58m2 au sol, soit 35,23m2 carrez + 39,35 m2 hors carrez (inférieur à 1m80) comprenant un GARAGE FERMÉ DE 12m2 attenant. Se composant d'une pièce à vivre de 23m2 intégrant cuisine équipée, à l'étage sous combles trois chambres en enfilade, salle de douche, deux wc. PARFAIT ÉTAT !

Fenêtres en double vitrage bois, volets bois, store électrique, chauffage individuel électrique, cumulus, climatisation réversible.

Pas de charges de copropriété, foncier à venir.

Prix 294000€, soit 280.000€ + 5%TTC d'honoraires à la charge de l'acquéreur



    ✓ Parking ouvert
    ✓ Parking fermé
    ✓ Jardin
    ✓ Terrasse
    ✓ Climatisation

C
A




33 avenue Alfred Borriglione 06100 NICE / Ligne 1 Arrêt BORRIGLIONE ou VALROSE

Tel. : 04 93 44 89 09 / Portable : 06 62 50 22 31 / isabelle@archimo-nice.com
