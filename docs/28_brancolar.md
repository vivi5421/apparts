# 28 Brancolar #

**Visite 25/7/2019 13h15**
**trop petit**



| Surface | Prix    | Pièces | Parking                                       |
| ------- | ------- | ------ | --------------------------------------------- |
| 47      | 260 000 | 3      | <ul><li>Garage</li><li>2 exterieurs</li></ul> |

![](https://www.century21.fr/imagesBien/202/558/c21_202_558_21583_8_EBDFACFB-E114-458C-9403-F2DF02F91463.jpg)

<https://www.century21.fr/trouver_logement/detail/1672097872/>

 260 000 € 

Description Réf: 21583

NICE - BAS BRANCOLAR - Maison indépendante dans copropriété de haut standing. Ravalée et entièrement rénovée, cette villa bénéficie d'un garage et d'un parking extérieur ainsi que d'une terrasse de 15m² + espace vert de 15m². Proche toutes commodités (écoles dont UFR Valrose, transports dont tramway, commerces...)



 Vue globale

    Surface totale : 47,2 m2
    Surface habitable : 45,0 m2
    Nombre de pièces : 3 [Voir en détail]

Equipement

    garage : 3place(s)
    Chauffage : Individuel au gaz individuel au gaz gaz
    Eau chaude : Individuelle gaz
    Double vitrage

Les Plus

    garage

À savoir

    Travaux récents : Tableau électrique, climatisation réversible, chaudière 2016, dble-vitrages
    Charges / mois : 95 €
    Taxe foncière : 1109 €

Les performances énergétiques

    280

    65






## Adresse ##
28 avenue de Brancolar

## Ecoles ##
VON DERWIES MATERNELLE
137 AVENUE SAINT LAMBERT -- 06000 NICE
Directeur : MUSSARD ISABELLE
Téléphone : 04 92 07 73 41
  
VON DERWIES ELEMENTAIRE
60 AVENUE ALFRED BORRIGLIONE -- 06100 NICE
Directeur : GARCIA PIERRE
Téléphone : 04 92 07 73 44


College Valeri

Lycee Imperial



## Contact ##
Century 21
04 92 09 23 23 

## Photos ##
![](https://www.century21.fr/imagesBien/202/558/c21_202_558_21583_1_EBDFACFB-E114-458C-9403-F2DF02F91463.jpg)
![](https://www.century21.fr/imagesBien/202/558/c21_202_558_21583_1_D6133ED5-3854-4C88-A503-4EC79CCC21F4.jpg)
![](https://www.century21.fr/imagesBien/202/558/c21_202_558_21583_1_0F4902B5-428C-4AD2-9AA1-AC2754105DF1.jpg)
![](https://www.century21.fr/imagesBien/202/558/c21_202_558_21583_1_75D04B21-D570-438E-AAFE-7699A68B41D1.jpg)
![](https://www.century21.fr/imagesBien/202/558/c21_202_558_21583_1_417B1348-1B10-445E-B58D-6335E836D0A4.jpg)
![](https://www.century21.fr/imagesBien/202/558/c21_202_558_21583_1_D18DD523-48DD-435F-A90F-9CF8989DC73F.jpg)
